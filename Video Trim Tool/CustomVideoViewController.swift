
import UIKit
import AVKit
import AVFoundation
import Photos

class CustomVideoViewController: UIViewController {

    
    
   
    @IBOutlet weak var croppedView: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnPause: UIButton!
    
    
    var url:NSURL! = nil
    var fetchResult: PHFetchResult<PHAsset>?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadLibrary()

    }
    
    
    
    @IBAction func playTapped(_ sender: UIButton) {
        
        
    }
    
    @IBAction func pauseTapped(_ sender: UIButton) {
        
        
    }
    
   
    
    func loadLibrary() {
        PHPhotoLibrary.requestAuthorization { (status) in
            if status == .authorized {
                self.fetchResult = PHAsset.fetchAssets(with: .video, options: nil)
            }
        }
    }
    
    func loadAssetRandomly() {
        guard let fetchResult = fetchResult, fetchResult.count > 0 else {
            print("Error loading assets.")
            return
        }
        
        let randomAssetIndex = Int(arc4random_uniform(UInt32(fetchResult.count - 1)))
        let asset = fetchResult.object(at: randomAssetIndex)
        PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (avAsset, _, _) in
            DispatchQueue.main.async {
                if let avAsset = avAsset {
                    self.loadAsset(avAsset)
                }
            }
        }
    }
    
    func loadAsset(_ asset: AVAsset) {
        // override in subclass
    }
    
}
